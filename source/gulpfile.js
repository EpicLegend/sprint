'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
   	watch = require('gulp-watch')
;

gulp.task('sass', function () {
	console.log("seen sass");
   return gulp.src('main.scss')
   		  .pipe(sass())
          .pipe(gulp.dest('../css/'));
});


gulp.task('watch', function () {
   gulp.watch('main.scss', gulp.series('sass'));
});


gulp.task('default', gulp.series('sass', 'watch'));