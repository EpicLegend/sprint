$(document).ready(function () {
  (function(){
    var show = true;
    var countbox = ".main__third";
    $(window).on("scroll load resize", function () {
        if (!show) return false; // Отменяем показ анимации, если она уже была выполнена
        var w_top = $(window).scrollTop(); // Количество пикселей на которое была прокручена страница
        var e_top = $(countbox).offset().top; // Расстояние от блока со счетчиками до верха всего документа
        var w_height = $(window).height(); // Высота окна браузера
        var d_height = $(document).height(); // Высота всего документа
        var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками
        if (w_top + 500 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {
            $('.number_anim').css('opacity', '1');
            $('.number_anim').spincrement({
                thousandSeparator: "",
                duration: 1200
            });
             
            show = false;
        }
    });
  })();


  // Координаты курсора
  ns4 = (document.layers)? true:false
  ie4 = (document.all)? true:false
  mapId = 0;
  function mousemove(event) {
      var mouse_x = y = 0;
      if (document.attachEvent != null) {
          mouse_x = window.event.clientX;
          mouse_y = window.event.clientY;
      } else if (!document.attachEvent && document.addEventListener) {
          mouse_x = event.clientX + 20;
          mouse_y = event.clientY + -30;
      }
      $("#mouse__position").css("display", "block");
      $("#mouse__position").html(arrRegion[ mapId ]);
      $("#mouse__position").css({ top: mouse_y, left: mouse_x});
  }

  // ховер свг карты
  $(".map__item").hover(function (e) {
    console.log();
    mapId = $(this).attr("data-id");
    document.onmousemove=mousemove;



  }, function () {
    document.onmousemove="";
    $("#mouse__position").css("display", "none");
  });

  var arrRegion = [
    "0",
    "Красноярск",
    "Новосибирск",
    "Омск",
    "Екатеринбург",
    "Пермь",
    "Челябинск",
    "Уфа",
    "Казань",
    "Самара",
    "Нижний Новгород",
    "Москва и МО",
    "Санк-Петербург",
    "Ростов на Дону",
    "Краснодар"];


});